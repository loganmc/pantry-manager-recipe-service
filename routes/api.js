const express = require('express');
const router = new express.Router();
const Axios = require('axios');
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/recipie', async (req, res, next) => {
  try {
    const ingredients = req.query.ingredients;

    const host = 'https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/findByIngredients';
    const number = req.query.number || 10;

    const result = await Axios.get(`${host}?number=${number}&ignorePantry=true&ingredients=${ingredients}`, {
      headers: {
        'X-RapidAPI-Host': 'spoonacular-recipe-food-nutrition-v1.p.rapidapi.com',
        'X-RapidAPI-Key': process.env.RAPID_API_KEY,
      },
    });
    res.status(200).json({data: result.data}).end();
  } catch (error) {
    next(error);
  }
});

router.post('/recipies/search', async (req, res, next) => {
  try {
    const token = req.headers['authorization'];
    const name = req.body.name;

    const ingredients = encodeURIComponent(name);

    const host = 'https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/findByIngredients';
    const number = req.query.number || 10;

    const result = await Axios.get(`${host}?number=${number}&ignorePantry=true&ingredients=${ingredients}`, {
      headers: {
        'X-RapidAPI-Host': 'spoonacular-recipe-food-nutrition-v1.p.rapidapi.com',
        'X-RapidAPI-Key': process.env.RAPID_API_KEY,
      },
    });

    const recipes = result.data;

    let html = `
    <h1>Suggested Recipes</h1>
    `;

    for (const recipe of recipes) {
      const recipeHtml = `
      <div class="recipe">
        <img src="${recipe.image}" alt="${recipe.title}" />
        <p>${recipe.title}</p>
        <a href="${process.env.WEBAPP_URL + '/recipe/' + recipe.id}">View</a>
      </div>
      `;
      html += recipeHtml;
    }

    const notificationResponse = await Axios.post(process.env.ACCOUNT_SERVICE_URL + '/api/notification', {
      subject: `Your ${name} is expiring!  Here's some recipes to try!`,
      html: html,
      text: html,
    }, {
      headers: {
        authorization: token,
      },
    });
    res.status(200).json({data: result.data}).end();
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get('/recipe/:recipieId', async (req, res, next) => {
  try {
    const recipeId = req.params.recipieId;
    const host = 'https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com';
    const result = await Axios.get(`${host}/recipes/${recipeId}/information`, {
      headers: {
        'X-RapidAPI-Host': 'spoonacular-recipe-food-nutrition-v1.p.rapidapi.com',
        'X-RapidAPI-Key': process.env.RAPID_API_KEY,
      },
    });

    res.json({
      recipe: result.data,
    }).end();
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
